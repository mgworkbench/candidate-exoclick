<?php
/**
 * Created by PhpStorm.
 * User: marc
 * Date: 26/11/16
 * Time: 16:21
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    'label' => 'label.name',
                    'required' => true,
                    'translation_domain' => 'messages',
                ]
            )
            ->add('costPrice', NumberType::class,
                [
                    'label' => 'label.price',
                    'required' => true,
                    'translation_domain' => 'messages',
                    'scale' => 2,
                ]
            )
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  'AppBundle\Entity\Ingredient'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_ingredient';
    }
}