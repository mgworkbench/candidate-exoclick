<?php
/**
 * Created by PhpStorm.
 * User: marc
 * Date: 26/11/16
 * Time: 12:15
 */

namespace AppBundle\Services;


use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Pizza;
use Doctrine\ORM\EntityManager;

class PizzaManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * PizzaManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Calculate the pizza's selling price
     *
     * Selling price equals the total of all its ingredients plus 50% of the total for the preparation
     *
     * @param Pizza $pizza
     * @return int|mixed
     */
    public function getPizzaSellingPrice(Pizza $pizza)
    {
        $sellingPrice = 0;

        if ($pizza->getIngredients()->count() > 0) {
            /** @var Ingredient $ingredient */
            foreach ($pizza->getIngredients() as $ingredient) {
                $sellingPrice += $ingredient->getCostPrice();
            }

            // add 50% for the preparation
            $sellingPrice += ($sellingPrice * 0.5);
        }

        return $sellingPrice;
    }
}