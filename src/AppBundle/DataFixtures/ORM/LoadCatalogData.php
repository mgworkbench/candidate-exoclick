<?php
/**
 * Created by PhpStorm.
 * User: marc
 * Date: 26/11/16
 * Time: 12:46
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Pizza;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCatalogData implements FixtureInterface, ContainerAwareInterface
{
    /** @var  ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Loading sample data from the test statement (The Fun Pizza & The Super Mushroom Pizza)
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // ingredients
        $ingredients = array(
            array('name' => 'tomato', 'costPrice' => 0.5),
            array('name' => 'sliced mushrooms', 'costPrice' => 0.5),
            array('name' => 'feta cheese', 'costPrice' => 1),
            array('name' => 'sausages', 'costPrice' => 1),
            array('name' => 'sliced onion', 'costPrice' => 0.5),
            array('name' => 'mozzarella cheese', 'costPrice' => 0.5),
            array('name' => 'oregano', 'costPrice' => 1),
            array('name' => 'bacon', 'costPrice' => 1),
        );

        // create ingredients
        foreach ($ingredients as $ingredient) {
            $pizzaIngredient = new Ingredient();

            $pizzaIngredient->setName($ingredient['name']);
            $pizzaIngredient->setCostPrice($ingredient['costPrice']);

            $manager->persist($pizzaIngredient);
        }

        $manager->flush();

        // pizza configuration
        $pizzaIngredients = array(
            array(
                'name' => 'The Fun Pizza',
                'ingredients' => array(
                    'tomato',
                    'sliced mushrooms',
                    'feta cheese',
                    'sausages',
                    'sliced onion',
                    'mozzarella cheese',
                    'oregano',
                ),
            ),
            array(
                'name' => 'The Super Mushroom Pizza',
                'ingredients' => array(
                    'tomato',
                    'bacon',
                    'mozzarella cheese',
                    'sliced mushrooms',
                    'oregano',
                ),
            ),
        );

        // create pizzas
        $pizzaManager = $this->container->get('app.pizza.manager');
        $ingredientRepository = $manager->getRepository('AppBundle:Ingredient');

        foreach ($pizzaIngredients as $pizzaComposition) {
            $pizza = new Pizza();

            $pizza->setName($pizzaComposition['name']);

            // add ingredients
            foreach ($pizzaComposition['ingredients'] as $ingredient) {
                $element = $ingredientRepository->findOneByName($ingredient);
                if ($element) {
                    $pizza->addIngredient($element);
                }
            }

            // get selling price
            $sellingPrice = $pizzaManager->getPizzaSellingPrice($pizza);
            $pizza->setSellingPrice($sellingPrice);

            $manager->persist($pizza);
        }

        $manager->flush();
    }
}