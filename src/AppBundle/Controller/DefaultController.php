<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Pizza;
use AppBundle\Form\IngredientType;
use AppBundle\Form\PizzaType;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // get current catalog to show at homepage
        $catalog = $this->getDoctrine()->getRepository('AppBundle:Pizza')->findAll();

        return $this->render('default/index.html.twig', [
            'catalog' => $catalog,
        ]);
    }

    /**
     * @Route("/list/ingredients", name="list_ingredients")
     */
    public function listIngredientsAction(Request $request)
    {
        $ingredients = $this->getDoctrine()->getRepository('AppBundle:Ingredient')->findAll();

        return $this->render('default/list_ingredient.html.twig', [
            'ingredients' => $ingredients,
        ]);
    }

    /**
     * @Route("/edit/ingredient/{id}", name="edit_ingredient")
     */
    public function editIngredientAction(Request $request)
    {
        $id = $request->get('id', null);
        $em = $this->getDoctrine()->getManager();

        $action = $this->get('translator')->trans('content.action.edit');

        if (0 == $id) {
            $ingredient = new Ingredient();
            $action = $this->get('translator')->trans('content.action.new');
        } elseif (0 != $id && null != $id) {
            $ingredient = $em->getRepository('AppBundle:Ingredient')->find($id);
        }

        if (null === $ingredient) {
            throw new EntityNotFoundException('Ingredient not found');
        }

        // create ingredient form
        $form = $this->createForm(IngredientType::class, $ingredient);

        // handle the form submitted
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $ingredient = $form->getData();

            $em->persist($ingredient);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('messages.success'));

            return new RedirectResponse($this->generateUrl('edit_ingredient', array('id'=>$ingredient->getId())));
        }

        return $this->render('default/edit_ingredient.html.twig', [
            'ingredient' => $ingredient,
            'form' => $form->createView(),
            'action' => $action,
        ]);
    }

    /**
     * @Route("/edit/pizza/{id}", name="edit_pizza")
     */
    public function editPizzaAction(Request $request)
    {
        $id = $request->get('id', null);
        $em = $this->getDoctrine()->getManager();

        $action = $this->get('translator')->trans('content.action.edit');

        if (0 == $id) {
            $pizza = new Pizza();
            $action = $this->get('translator')->trans('content.action.new');
        } elseif (0 != $id && null != $id) {
            $pizza = $em->getRepository('AppBundle:Pizza')->find($id);
        }

        if (null === $pizza) {
            throw new EntityNotFoundException('Pizza not found');
        }

        // create pizza form
        $form = $this->createForm(PizzaType::class, $pizza);

        // handle the form submitted
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Pizza $pizza */
            $pizza = $form->getData();

            $pizzaManager = $this->get('app.pizza.manager');

            $newSellingPrice = $pizzaManager->getPizzaSellingPrice($pizza);
            $pizza->setSellingPrice($newSellingPrice);

            $em->persist($pizza);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('messages.success'));

            return new RedirectResponse($this->generateUrl('edit_pizza', array('id'=>$pizza->getId())));
        }

        return $this->render('default/edit_pizza.html.twig', [
            'pizza' => $pizza,
            'form' => $form->createView(),
            'action' => $action,
        ]);
    }
}
