Exoclick Test
=============

This project has been developed with Symfony 3.1.7 and Bootstrap to add some style

To install the project just run the next command in the console:
    composer install

Database requeriments:
    For data model, this project needs have a database, the default name is 'pizza_catalog'
    The user for this database must be called 'exoclick' and the password, 'exoclick' too.
    If you want create the database schema just run the next command in the console:
        bin/console doctrine:schema:update --force

Some dummy data have been coded, to install it, just run this command in the console:
    bin/console doctrine:fixtures:load

No extra features has been done. Just, list a catalog, ingredients and edit the pizzas from catalog and the ingredients.

Features like delete pizzas or ingredients from catalog, orders, delivery, etc have remained in mind for a future development

To see the project(in a development environment and without any server configuration), just run the next command in the console:
    bin/console server:run

I have had some problems of time to have more availability to perform the test.